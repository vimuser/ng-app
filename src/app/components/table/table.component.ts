import { Component, OnInit, HostListener } from '@angular/core';
import { StackoverflowService } from '../../services/stackoverflow/stackoverflow.service';
import { MessageService } from '../../services/message/message.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  items = [];
  private isLoading = false;
  private pageId = 1;

  constructor(
    private stackoverflowService: StackoverflowService,
    private messageService: MessageService
  ) { }

  @HostListener('window:scroll')
  handleScroll() {
    if (!this.isLoading && this.isFireScrollPointReached()) {
      this.pageId++;
      this.fetchQuestions();
    }
  }

  ngOnInit() {
    this.fetchQuestions();
  }

  private fetchQuestions() {
    this.isLoading = true;
    this.stackoverflowService.fetchQuestions(this.pageId)
      .subscribe(({items}: any) => {
        this.isLoading = false;
        this.items = this.items.concat(items);
      });
  }

  private isFireScrollPointReached() {
    const {innerHeight, scrollY} = window;
    const documentOffsetHeight = document.body.offsetHeight;
    return ((innerHeight + scrollY) - documentOffsetHeight) > (documentOffsetHeight * 0.8 - documentOffsetHeight);
  }

  private handleRowClick(item) {
    const {title, link} = item;
    this.messageService.sendMessage({
      event: 'showModal',
      data: {
        title,
        link
      }
    });
  }

}
