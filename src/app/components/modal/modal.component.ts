import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Input, Renderer2, Inject } from '@angular/core';
import { MessageService } from '../../services/message/message.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  data;

  constructor(
    private messageService: MessageService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit() {
    this.messageService.getMessage().subscribe(({event, data}: any) => {
      if (event === 'showModal') {
        this.renderer.addClass(this.document.body, 'modal-open');
        this.data = data;
      }
    });
  }

  closeModal() {
    this.data = '';
    this.renderer.removeClass(this.document.body, 'modal-open');
  }

}
