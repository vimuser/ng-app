import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StackoverflowService {
  constructor(
    private httpClient: HttpClient
  ) { }

  fetchQuestions(id) {
    return this.httpClient.get(this.getApi(id));
  }

  private getApi(id) {
    return `https://api.stackexchange.com/2.2/questions?page=${id}&order=desc&sort=activity&site=stackoverflow`;
  }
}
